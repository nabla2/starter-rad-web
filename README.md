SPA-REST API プロジェクトサンプル
=================================

前提条件
---------
以下がインストールされていること

- JDK(1.8)
- gradle (最新版)
- NodeJS (最新版)

使用方法
---------

1. `build.gradle`と`project.xlsx`を適当なディレクトリに配置する。

2. ターミナルで上記ディレクトリに移動

3. 下記のコマンドを順次実行

  ```bash
  > gradle
  > npm install
  > npm run build
  > gradle bootrun
  ```

4. ブラウザで下記URLにアクセス

http://localhost:8080/swagger-ui.html
(Web-APIテストページ)

http://localhost:8080/
(Angular4クライアント)

